package com.atlassian.pageobjects.elements.util;

import com.atlassian.pageobjects.elements.mock.clock.AbstractMockClock;

import java.time.Duration;
import java.time.Instant;

/**
 * This {@link java.time.Clock} moves forward every time its {@link #instant()} method is called.
 */
public class IncrementalClock extends AbstractMockClock
{
    private final Duration step ;
    private Instant time ;

    public IncrementalClock(long step) {
        this.time = Instant.EPOCH;
        this.step = Duration.ofMillis(step);
    }

    @Override
    public Instant instant() {
        Instant currentTime = time;
        time = time.plus(step);
        return currentTime;
    }
}
