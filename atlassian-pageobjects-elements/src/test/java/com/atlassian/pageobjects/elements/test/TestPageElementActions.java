package com.atlassian.pageobjects.elements.test;

import com.atlassian.pageobjects.elements.GlobalElementFinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.test.pageobjects.page.ElementsPage;
import com.atlassian.webdriver.testing.annotation.WindowSize;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;

// This test requires elements to be in view since the W3C WebDriver spec does not allow elements to be scrolled into view
// using Actions. See https://github.com/w3c/webdriver/issues/1005. This annotation normalizes the browser screen size
// across all browsers.
@WindowSize(width = 800, height = 600)
public class TestPageElementActions extends AbstractPageElementBrowserTest
{
    private PageElementFinder elementFinder;

    @Before
    public void initFinder()
    {
        product.visit(ElementsPage.class);
        elementFinder = product.getPageBinder().bind(GlobalElementFinder.class);
    }

    @Test
    public void shouldGetActionsViaContext()
    {
        final PageElementActions actions = product.getPageBinder().bind(PageObjectWithActions.class).actions;
        assertNotNull(actions);
        testActions(actions);
    }

    @Test
    public void shouldGetActionsViaPageBinder()
    {
        final PageElementActions actions = product.getPageBinder().bind(PageElementActions.class);
        assertNotNull(actions);
        testActions(actions);
    }

    private void testActions(PageElementActions actions)
    {
        final PageElement firstButton = elementFinder.find(By.id("test1_addElementsButton"));
        final PageElement secondButton = elementFinder.find(By.id("test2_toggleInputVisibility"));
        final PageElement input = elementFinder.find(By.id("test5_input"));
        // just make sure it won't blow for now
        actions.moveToElement(firstButton).click()
                .click(secondButton)
                .clickAndHold()
                .release()
                .clickAndHold(firstButton)
                .release(firstButton)
                .click(input)
                .doubleClick()
                .doubleClick(firstButton)
                .contextClick(secondButton)
                .sendKeys(Keys.ESCAPE) // Closes the context menu
                .dragAndDrop(firstButton, secondButton)
                .dragAndDropBy(secondButton, 10, 10)
                .keyDown(Keys.ALT)
                .keyUp(Keys.ALT)
                .keyDown(firstButton, Keys.CONTROL)
                .keyUp(firstButton, Keys.CONTROL)
                .moveByOffset(100, 100)
                .moveToElement(firstButton)
                .moveToElement(secondButton, 10, 10)
                .click(input)
                .sendKeys("blah")
                .click(firstButton)
                .sendKeys(input, "moreblah")
                .perform();
    }

    public static class PageObjectWithActions
    {
        @Inject
        private PageElementActions actions;
    }

}
