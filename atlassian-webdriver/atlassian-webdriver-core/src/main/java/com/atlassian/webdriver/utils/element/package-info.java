/**
 * <p>
 * WebDriver Poller utility.
 * </p>
 *
 * <p>
 * WARNING: deprecated since 3.0.0 for removal in 4.0.0. Use {@code Poller} from the
 * 'atlassian-pageobjects-elements' module instead.
 * </p>
 * @since 2.1.0
 */
@Deprecated
package com.atlassian.webdriver.utils.element;