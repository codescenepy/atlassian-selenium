package com.atlassian.webdriver.testing.rule;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.Statement;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.FileDetector;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import static org.junit.runner.Description.EMPTY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LocalFileDetectorRuleTest {

    @Mock
    private RemoteWebDriver remoteWebDriver;

    @Mock
    private WebDriver webDriver;

    @Mock
    private Statement statement;

    @Test
    public void testLocalWebDriverIsNotUpdated() throws Throwable {
        new LocalFileDetectorRule(webDriver).apply(statement, EMPTY).evaluate();

        verify(remoteWebDriver, never()).setFileDetector(any());
    }

    @Test
    public void testRemoteWebDriverIsUpdated() throws Throwable {
        new LocalFileDetectorRule(remoteWebDriver).apply(statement, EMPTY).evaluate();

        verify(remoteWebDriver).setFileDetector(any(LocalFileDetector.class));
    }

    @Test
    public void testRemoteWebDriverFileDetectorIsRestoredAfterRun() throws Throwable {
        FileDetector previousFileDetector = mock(FileDetector.class);
        when(remoteWebDriver.getFileDetector()).thenReturn(previousFileDetector);
        doAnswer(invocation -> {
            verify(remoteWebDriver).getFileDetector();
            verify(remoteWebDriver).setFileDetector(any(LocalFileDetector.class));
            verifyNoMoreInteractions(remoteWebDriver);
            return null;
        }).when(statement).evaluate();

        new LocalFileDetectorRule(remoteWebDriver).apply(statement, EMPTY).evaluate();

        verify(remoteWebDriver).setFileDetector(previousFileDetector);
        verifyNoMoreInteractions(remoteWebDriver);
    }

}