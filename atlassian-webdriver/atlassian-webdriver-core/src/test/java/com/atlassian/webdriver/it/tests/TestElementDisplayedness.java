package com.atlassian.webdriver.it.tests;

import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.page.ElementDisplayednessPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * These tests are written to demo {@link WebDriver} behavior, and to be informed if/when assumptions change.
 *
 * They serve as a catalog of things that do and do not work to hide a {@link WebElement}. If developers are in doubt,
 * they can refer to these tests so they can see what works for themselves.
 *
 * We have no opinion on what {@link WebDriver} <strong>should</strong> be doing - that's up to its developers. However,
 * we want to know when they change their minds, especially if it affects the way our developers write their tests and
 * what assumptions they often make.
 *
 * If tests here were passing in older versions of {@link WebDriver} and failing in a newer one, that's good for us to
 * know; we'll need to add that information to an upgrade guide, then inform developers as part of the upgrade that this
 * kind of testing technique has changed or doesn't work anymore, etc.
 */
public class TestElementDisplayedness extends AbstractSimpleServerTest {
    private WebDriver driver;

    @Before
    public void init() {
        product.visit(ElementDisplayednessPage.class);
        driver = product.getTester().getDriver();
    }

    @Test
    public void testVanillaElementIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#plain-visible input"));
        assertThat(el.isDisplayed(), is(true));
    }

    @Test
    public void testElementWithZeroOpacityIsInvisible() {
        WebElement el = driver.findElement(By.cssSelector("#zero-opacity input"));
        assertThat(el.isDisplayed(), is(false));
    }

    @Test
    public void testElementOutsideOverflowBoundaryIsInvisible() {
        WebElement el = driver.findElement(By.cssSelector("#pseudo-checkbox-1 input"));
        assertThat(el.isDisplayed(), is(false));
    }

    @Test
    public void testElementWithNegativeZIndexIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#negative-z-index input"));
        assertThat(el.isDisplayed(), is(true));
    }

    @Test
    public void testElementWithClipTrickIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#pseudo-checkbox-2 input"));
        assertThat(el.isDisplayed(), is(true));
    }

    @Test
    public void testElementWithAggressiveClipTrickIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#pseudo-checkbox-3 input"));
        assertThat(el.isDisplayed(), is(true));
    }
}
